<?php

namespace Drupal\prefetcher\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Prefetcher uri edit forms.
 *
 * @ingroup prefetcher
 */
class PrefetcherUriForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\prefetcher\Entity\PrefetcherUri */
    $form = parent::buildForm($form, $form_state);

    $entity = $this->entity;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = &$this->entity;

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Prefetcher uri.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Prefetcher uri.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.prefetcher_uri.canonical', ['prefetcher_uri' => $entity->id()]);
  }

}
