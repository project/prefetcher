<?php

namespace Drupal\prefetcher\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Prefetcher uri entities.
 *
 * @ingroup prefetcher
 */
class PrefetcherUriDeleteForm extends ContentEntityDeleteForm {


}
