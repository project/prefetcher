<?php

namespace Drupal\prefetcher;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for prefetcher_uri.
 */
class PrefetcherUriTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
